import { Request, Response } from "express";
import * as Contacts from '../models/contact_model';

/** 
 * This function return all from one user by id
 * @params_id 
*/
export function getUserById(req : Request, res : Response) {
    Contacts.getUserById(req.params.id)
    .then((rows : any)=> {
        if (!rows[0]){
            res.status(404).json({ "message" : "This user's id doesn't exists."});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function return user's name
 * @params_id 
*/
export function getUserByName(req : Request, res : Response) {
    Contacts.getUserByName(req.params.id)
    .then((rows : any)=> {
        if (!rows[0]){
            res.status(404).json({ "message" : "This user's name doesn't exists."});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function return user's first name
 * @params_id 
*/
export function getUserByFirstName(req : Request, res : Response) {
    Contacts.getUserByFirstName(req.params.id)
    .then((rows : any)=> {
        if (!rows[0]){
            res.status(404).json({ "message" : "This user's first name doesn't exists."});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function return user's phone number
 * @params_id 
*/
export function getUserByPhone(req : Request, res : Response) {
    Contacts.getUserByPhone(req.params.id)
    .then((rows : any)=> {
        if (!rows[0]){
            res.status(404).json({ "message" : "This user's phone number doesn't exists."});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function return user's mail
 * @params_id 
*/
export function getUserByMail(req : Request, res : Response) {
    Contacts.getUserByMail(req.params.id)
    .then((rows : any)=> {
        if (!rows[0]){
            res.status(404).json({ "message" : "This user's mail doesn't exists."});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function update user's name
 * @params_id
 * @params_body
*/
export function updateNameFromUser(req : Request, res : Response) {
    Contacts.updateNameFromUser(req.params.id, req.body)
    .then((rows : any)=>{
        if (rows.affectedRows === 0){
            res.status(404).json({ "message" : "Update failed"});
        } else {
            res.json(req.body);
        };
    })
    .catch((err : any)=>res.status(500).send(err))
};

/** 
 * This function update user's first name
 * @params_id
 * @params_body
*/
export function updateFirstNameFromUser(req: Request, res: Response) {
    Contacts.updateFirstNameFromUser(req.params.id, req.body)
    .then((rows : any)=>{
        if (rows.affectedRows === 0){
            res.status(404).json({ "message" : "Update failed"});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err))
};

export function updatePhoneFromUser(req: Request, res: Response) {
    Contacts.updatePhoneFromUser(req.params.id, req.body)
    .then((rows : any)=>{
        if (rows.affectedRows === 0){
            res.status(404).json({ "message" : "Update failed"});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err))
};

export function updateMailFromUser(req: Request, res: Response) {
    Contacts.updateMailFromUser(req.params.id, req.body)
    .then((rows : any)=>{
        if (rows.affectedRows === 0){
            res.status(404).json({"message" : "Update failed"});
        } else {
            res.json(rows);
        };
    })
    .catch((err : any)=>res.status(500).send(err))
};