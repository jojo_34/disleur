// CONFIG.TS

import dotenv from "dotenv";
import mysql, { ConnectionOptions } from 'mysql2';

dotenv.config();
const { DB_HOST, DB_USER, DB_PASSEWORD, DB_NAME } = process.env;

const access: ConnectionOptions = {

      host: DB_HOST,
      user: DB_USER,
      password: DB_PASSEWORD,
      database: DB_NAME,
};

export const conn = mysql.createConnection(access);
