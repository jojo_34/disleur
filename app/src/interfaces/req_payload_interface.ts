import jsonwebtoken from 'jsonwebtoken';

export interface Reqpayload extends Request {

      payload?: string | jsonwebtoken.JwtPayload ;
}