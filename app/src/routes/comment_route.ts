import { Router } from 'express';
const commentRouter = Router();
import * as controller from '../controllers/comment_controller';

// all comments for one user + test d'authentification
commentRouter.get("/user/:id", controller.getAllComments);
// one comment 
commentRouter.get("/:id", controller.getOneCommentById);
// all comments for one article
commentRouter.get("/article/:id", controller.getAllCommentByArticle);
// all comments for one article by date and quantity limit
commentRouter.get("/last/article/:id", controller.getLastCommentsByDate);
// update comment
commentRouter.put("/:id", controller.updateComment);
// delete comment
commentRouter.delete("/:id", controller.deleteComment);
// create comment
commentRouter.post("/", controller.createComment);

export default commentRouter;
