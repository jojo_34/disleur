import { Request, Response } from 'express';
import * as Articles from "../models/article_model";

/** 
This function returns all articles posted by a writer user
 * @params_id 
*/
export function getAllArticles(req : Request, res : Response){

    Articles.getAllArticlesOrderByDate()
    .then((rows: any)=>res.json(rows))
    .catch((err: any)=>res.status(500).send(err));
};

/** 
This function returns one category related to an article
 * @params_id 
*/

export function getAllArticlesByCategory(req : Request, res : Response){

      Articles.getAllArticlesByCategory(req.params.id)
      .then((rows : any)=> {

            if (!rows[0]){

                  res.status(207).json({ "message" : "Aucun article dans cette catégorie" });
            } else {

                  res.json(rows);
            }
     })     
     .catch((err : any)=>res.status(500).send(err));
};


/** 
This function returns one article by id posted by a writer user
 * @params_id 
*/
export function getOneArticleById(req : Request, res : Response){
    
    Articles.getOneArticleById(req.params.id)
   .then((rows: any)=> res.json(rows))
   .catch((err: any)=>res.status(500).send(err));
};

/** 
 * This function update an article posted by a writer user
 * @params_id 
 * @params_body
*/
export function updateArticle(req : Request, res : Response){
    Articles.updateArticle(req.params.id, req.body)
    .then((rows : any)=>res.json(rows))
    .catch((err : any)=>res.status(500).send(err));
}

/** 
 * This function remove one article
 * @params_id 
*/
export function deleteArticle(req : Request, res : Response){

    Articles.deleteArticle(req.params.id)
    .then((rows : any)=>{

        console.log('rows.affectedRows :', rows.affectedRows);

        if (rows.affectedRows === 0){

                res.status(404).json({ "message" : 'Aucun article à supprimer !' });
        } else {

                res.status(207).json({ "message" : "Article supprimé !" })
        }
    })
    .catch((err : any)=>res.status(500).send(err));
}

/** 
 * This function create a new article
 * @params_body
*/
export function createArticle(req : Request, res : Response){
    Articles.createArticle(req.body)
    .then((rows : any)=>res.json(rows))
    .catch((err : any)=>res.status(500).send(err));
}






