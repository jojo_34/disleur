#!/bin/bash
###############################################################
# Définition des variables
ctnName="disleur_mysql_ctn";
dbUser="root";
dbPswd="mysqlpwd";
dbName="disleur_db";
###############################################################

# Affichage de l'action en cours
echo "Import $1 file to $dbName database of $ctName container";

# Copie du fichier SQL depuis la machine hôte vers le conteneur
docker cp $1 $ctnName:/tmp;

# Importation du fichier SQL dans la base de données du conteneur
docker exec -w /tmp $ctnName bash -c "mysql -u$dbUser -p$dbPswd < $1";

# Suppression du fichier SQL temporaire dans le conteneur
docker exec -w /tmp $ctnName bash -c "rm -f $1";

# Sortie avec succès
exit 0;
# Lancer le script=> ./import_db.sh backup_date+heure.sql => Exemple