// Imports the router functionality of the express module
import { Request, Response } from "express";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken"
import { IUser } from "../interfaces/user_interface"

// Imports the functions from user_model
import * as Users from "../models/user_model";
import { RowDataPacket } from "mysql2";
//import hash from "../services/hash";


/**
 * This function return the list of all users
 * @param _req 
 * @param res 
 */
export function getAllUsers(_req : Request, res: Response) {
    Users.getAllUsers()
    .then((rows : any) => res.json(rows))     
    .catch((err : any) => res.status(500).send(err))
};

/**
 * This function return one user
 * @param req 
 * @param res 
 */
export function getOneUserById(req : Request, res: Response) {
    Users.getOneUserById(req.params.id)
    .then((rows: any) => {
        if(rows <= 0) {
            res.status(404).json({
                "message" : "not found."
            });
        } else {
            res.json(rows);
        }
    })
    .catch((err: any)=> res.status(500).send(err));
};

/**
 * This function update the name of one user
 * @param req 
 * @param res 
 */
export function updateUser(req : Request, res : Response) {
    Users.updateUser(req.params.id, req.body)
    .then((rows : any) => {
        if(rows.affectedRows === 0) {
            res.status(404).json({
                msg : "Update failed"
            });
        } else {
            res.json({
                msg : "Update success"
            });
        }
    })
    .catch((err : any) => res.status(500).send(err));
};

/**
 * This function remove one user
 * @param req 
 * @param res 
 */
export function deleteUser(req : Request, res : Response) {
    Users.deleteUser(req.params.id)
    .then((rows : any) => {
        if(rows.affectedRows === 0) {
            res.status(404).json({
                msg : "Delete failed"
            });
        } else {
            res.json({
                msg : "Delete success"
            });         
        }
    })
    .catch((err : any) => res.status(500).send(err));  
};

/**
 * This function create a new user after to have verified if the mail exists
 * Call getAllCategories() to verify any existing mail
 * @param req 
 * @param res 
 */
export async function createUser(req : Request, res : Response) {

    let hash = bcrypt.hashSync(req.body.password, 10);
    console.log(hash)
    req.body.password = hash;
                 
            await Users.createUser(req.body)
            .then((rows : RowDataPacket) => {
                if(rows.affectedRows === 0) {
                    res.status(404).json(rows);
                } else {
                    res.json(rows)
                    
                } if(rows.length) {
                    let idUser = rows[0].id_user;
                    const token = jwt.sign(idUser, "SECRETKEY", { expiresIn: "1h" });
                    console.log(token);
                    return { token: token };                    
                };  
            })
            .catch((err : any) => {
                if(err.code === "ER_DUP_EMPTY") {
                    res.status(400).json({
                        err : "User exist",
                        Full_Error : err
                    });
                } else {
                    res.status(400).json({err : err.message});
                }
            });          
};