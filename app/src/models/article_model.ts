import {conn} from "../config/config";

/**
 * This function returns all articles ordered by date with a limit of 15,
 * its used on the page "Tous les articles" (without distinction),
 * and also get more informations related to each article as the author,
 * the article's category, his picture ... etc.
 */
export async function getAllArticlesOrderByDate(): Promise<any> {
  const [rows] = await conn.promise().execute(
    `SELECT Articles.id_article, Articles.title, Articles.text, Articles.date, Articles.like_qty, Articles.share_qty, Articles.state_article, \
    Users.lastname, Users.firstname, \
    Pictures.picture_title, Pictures.legend, Pictures.meta_data, Pictures.url, Pictures.id_picture, \
    Categories.name \
    FROM Articles \
    JOIN Articles_users ON Articles_users.id_article_user = Articles.id_article \
    JOIN Users ON Articles_users.id_user = Users.id_user \
    JOIN Pictures ON Pictures.id_article = Articles.id_article \
    JOIN Articles_categories ON Articles_categories.id_article = Articles.id_article \
    JOIN Categories ON Articles_categories.id_category = Categories.id_category \
    
    LIMIT 15`
  );
  return rows;
}

/** 
 * 
 *This function returns one category on dataBase from one article 
 * @params_id 
*/
export async function getAllArticlesByCategory(id: string) : Promise <any> {

      const [rows] = await conn.promise().execute(
        `SELECT Articles.id_article, Articles.title \
        FROM Articles \
        INNER JOIN Articles_categories \
        ON Articles.id_article = Articles_categories.id_article \
        INNER JOIN Categories \
        ON Articles_categories.id_category = Categories.id_category WHERE Categories.name = ?`,
        [id]);  
      return rows;
};

/** 
 * 
This function returns one article found on the database
 * @params_id 
*/
export async function getOneArticleById(id : string): Promise <any>{
    
    const [rows] = await conn.promise().execute(
    `SELECT Articles.id_article, Articles.title, Articles.text, Articles.date, Articles.like_qty, Articles.share_qty, Articles.state_article, \
    Users.lastname, Users.firstname, \
    Pictures.picture_title, Pictures.legend, Pictures.meta_data, Pictures.url, \
    Categories.name \
    FROM Articles \
    JOIN Users ON Articles.id_article = Users.id_user \
    JOIN Pictures \
    ON Articles.id_article = Pictures.id_picture \
    JOIN Categories \
    ON Articles.id_article = Categories.id_category \
    WHERE Articles.id_article = ?`, 
    [id]);
    return rows;    
};

/** 
 * This function Update one article on database
 * @params_body
 * @params_id 
*/
export async function updateArticle(id : string, body: { title: string, text: string }) : Promise <any> {
    /**  Cette fonction modifie uniquement les clés de la table Articles 
     * => à refaire pour inclure 2 clés de la table Users (lastname/firstname) et 
     * des clés de la table Pictures
     * */
    const [rows] = await conn.promise().execute(
        `UPDATE Articles \
        SET title = '${body.title}', text = '${body.text}' \
        WHERE id_article = ?`, [id]
    );
    return [rows, [body]];
};

/** 
 * This function Delete one article on database
 * @params_id 
*/
export async function deleteArticle(id : string) : Promise <any> {
  
    const [rows] = await conn.promise().execute(
        `DELETE FROM Articles WHERE id_article = ?`, [id]
    );
    return rows;
};

/** 
 * This function create a new article on database
 * @params_body
*/
export async function createArticle(body: any) : Promise <any> {

    const {title, text, date, like_qty, share_qty, state_article} = body;

    const [rows] = await conn.promise().execute(
        `INSERT INTO Articles (title, text, date, like_qty, share_qty, state_article) \
        VALUE (?, ?, ?, ?, ?, ?)`,
        [title, text, date, like_qty, share_qty, state_article]
    );
    return rows;
};
    /** Crud Create à revoir car fonctionne dans PMA mais pas dans VSCode
     * Devrait permettre d'inclure des clés des tables Users et Categories
     * Dans config.ts => multipleStatements: true, devrait permettre de faire des req multiples
     */
    // (
    //     `INSERT INTO Articles (title, text, date, like_qty, share_qty, state_article) \
    //     VALUES (?, ?, ?, ?, ?, ?); \
    //     SET @newArticleId=LAST_INSERT_ID(); \
    //     INSERT INTO Articles_users (id_article, id_user) \
    //     VALUES (@newArticleId, ?); \
    //     INSERT INTO Articles_categories (id_article, id_category) \
    //     VALUES (@newArticleId, ?);`,
    //     ['DERT', 'TRED', '2024-01-10', 0, 0, 1, 1, 1]
    // );

conn.connect(function(err: any) { err ? err : console.log("Connecté à la base de données MySQL!"); });