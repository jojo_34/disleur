import { Router } from 'express';
const categoryRouter = Router();
import * as controller from '../controllers/category_controller';

categoryRouter.get("/", controller.getAllCategories);
categoryRouter.get("/:id", controller.getOneCategoryById);
categoryRouter.put("/:id", controller.updateCategory);
categoryRouter.delete("/:id", controller.deleteCategory);
categoryRouter.post("/", controller.createCategory);

export default categoryRouter;