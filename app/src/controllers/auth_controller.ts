import { Request, Response } from 'express';
import * as Auth from "../models/auth_model";

/** 
 * This function is used for signIn when the user already have an account
 * @params_body
*/
export function signIn(req : Request, res : Response): void{
    
      Auth.signIn(req.body)
      .then((sql: any)=>res.json(sql))
      .catch((err : any)=>res.status(500).send(err));
}

