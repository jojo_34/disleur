#!/bin/bash
###############################################################
# Définition des variables
ctnName="disleur_mysql_ctn";
dbUser="root";
dbPswd="mysqlpwd";
dbName="disleur_db";
###############################################################

# Affichage de l'action en cours
echo "Export $dbName database from $ctnName container";

# Exportation de la base de données vers un fichier SQL local
docker exec -w /tmp $ctnName bash -c "mysqldump -u$dbUser -p$dbPswd -B $dbName > backup.sql";

# Copie du fichier de sauvegarde depuis le conteneur vers la machine hôte
docker cp $ctnName:/tmp/backup.sql ./;

# Suppression du fichier de sauvegarde temporaire dans le conteneur
docker exec -w /tmp $ctnName bash -c "rm -f backup.sql";

# Renommage du fichier de sauvegarde avec la date et l'heure actuelles
backup_name=backup_$(date +%Y%m%d_%H%M%S).sql;
mv backup.sql $backup_name;

# Sortie avec succès
exit 0;
# Lancer la commande suivante dans le terminal pour exectuer le script => ./export_db.sh 