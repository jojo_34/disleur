import { Request, Response } from "express";
import { RowDataPacket } from "mysql2";
import * as Categories from "../models/category_model";
// import bcript from "bcryptjs";

/** 
 * This function return the list of all categories
 * Its used into the menu burger
*/
export async function getAllCategories(_req : Request ,res : Response){

    await Categories.getAllCategories()
    .then((rows : RowDataPacket)=>{
        if(!rows[0]) { res.status(404).json({ msg : "Categories not found !" });
        }else { res.json(rows) };
    })
    .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function return one category
 * @params_id 
*/
export async function getOneCategoryById(req : Request, res : Response){

    await Categories.getOneCategoryById(req.params.id)
   .then((rows : RowDataPacket)=>{
        if(!rows[0]) { res.status(404).json({ msg : "This category doesnt exists !" });
        }else { res.json(rows) };
   })
   .catch((err : any)=>res.status(500).send(err));
};

/** 
 * This function update the name of one category
 * @param _id 
 * @params_body
*/
export async function updateCategory(req : Request, res : Response){

        await Categories.updateCategory(req.params.id, req.body)
        .then((rows : RowDataPacket)=>{
            if (rows.affectedRows === 0){ res.status(404).json({msg : "Update failed"});    
            }else { res.json(req.body); }
        })
        .catch((err : any)=>res.status(500).send(err));       
}

/** 
 * This function remove one category
 * @params_id 
*/
export function deleteCategory(req : Request, res : Response){
    Categories.deleteCategory(req.params.id)
    .then((rows : RowDataPacket)=>{
        if (rows.affectedRows === 0){ res.status(404).json({msg : "Delete failed"});    
        }else { res.json({msg : "Delete success"}); }
    })
    .catch((err : any)=>{
        if(err.code === "ER_ROW_IS_REFERENCED_2"){
            res.status(400).json({
                err : "cannot delete a category that contains articles", 
                Full_Error : err
            });
        }else{
            res.status(400).json({err : err.message});
        }
    });
}

/** 
 * This function create a new category after to have verified if the name exists
 * Call getAllCategories() to verify any existing name
 * @params_body 
*/
export async function createCategory(req : Request, res : Response){

    // To hash data before to insert it in database
    // let hash = bcript.hashSync(req.body.name, 10);
    // console.log(hash)
    // req.body.name = hash;

        await Categories.createCategory(req.body)
        .then((rows : RowDataPacket)=>{
            if(rows.affectedRows === 0) { res.status(404).json(rows);
            }else { res.json( rows)};
        })
        .catch((err : any)=>{
            if(err.code === "ER_DUP_ENTRY"){
                res.status(400).json({
                    err : "Already exists", 
                    Full_Error : err
                });
            }else{
                res.status(400).json({err : err.message});
            }
        });
}






