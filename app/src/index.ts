import express, { Express } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';

import categoryRouter from './routes/category_route';
import articleRouter from './routes/article_route';
import contactRouter from './routes/contact_route';
import commentRouter from './routes/comment_route';
import userRouter from "./routes/user_route";
import authRouter from './routes/auth_route';

const app: Express = express();
dotenv.config(); 
const { API_PORT } = process.env;

app.use(bodyParser.json());
app.use(cors());

app.use('/categories', categoryRouter);
app.use("/articles", articleRouter);
app.use('/contacts', contactRouter);
app.use('/comments', commentRouter);
app.use("/users", userRouter);
app.use('/auth', authRouter);

app.listen(API_PORT, (): void => {

    console.log("L'appli tourne sur le port " + API_PORT);
});