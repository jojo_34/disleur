// Imports the router functionality of the express module
import { Router } from "express";

// Imports the functions from user_controller
import * as controller from "../controllers/user_controller";

// Declare the routeUsers constant in which we store the return of the router function
const userRouter: Router = Router();

// Declare the routeUsers constant in which we store the return of the router function
// Route to read all users
userRouter.get("/", controller.getAllUsers);
// Route to read user by id
userRouter.get("/:id", controller.getOneUserById);
// Route to update user
userRouter.put("/:id", controller.updateUser);
// Route to deleted user
userRouter.delete("/:id", controller.deleteUser);
// Route to create user
userRouter.post("/", controller.createUser);

export default userRouter;