import { Router } from 'express';
const articleRouter = Router();
import * as controller from "../controllers/article_controller";

articleRouter.get("/", controller.getAllArticles);
articleRouter.get("/category/:id", controller.getAllArticlesByCategory);
articleRouter.get("/:id", controller.getOneArticleById);
articleRouter.put("/:id", controller.updateArticle);
articleRouter.delete("/:id", controller.deleteArticle);
articleRouter.post("/", controller.createArticle);

export default articleRouter;

