import { Router } from "express";
const contactRouter = Router();
import * as controller from '../controllers/contact_controller';

contactRouter.get("/:id", controller.getUserById);
contactRouter.get("/:id/name", controller.getUserByName);
contactRouter.get("/:id/firstname", controller.getUserByFirstName);
contactRouter.get("/:id/phone", controller.getUserByPhone);
contactRouter.get("/:id/mail", controller.getUserByMail);

contactRouter.put("/:id", controller.updateNameFromUser);
contactRouter.put("/:id/firstname", controller.updateFirstNameFromUser);
contactRouter.put("/:id/phone", controller.updatePhoneFromUser);
contactRouter.put("/:id/mail", controller.updateMailFromUser);

export default contactRouter;