import {conn} from "../config/config";
import { RowDataPacket } from "mysql2";
import { ICategory } from "../interfaces/interface_category";

let rows : RowDataPacket[];

/** 
 * This function return the categories's list found on database
*/
export async function getAllCategories() : Promise <any>{
    [rows] =  await conn.promise().execute<RowDataPacket[]>("SELECT * FROM Categories");
    return  rows;
};

/** 
 * This function return one category found on database
 * @params_id 
*/
export async function getOneCategoryById(id: string) : Promise <any> {
    [rows] = await conn.promise().execute<RowDataPacket[]>("SELECT * FROM Categories WHERE id_category = ?", [id] );
    return rows;
};

/** 
 * This function Update the name of one category on database
 * @params_id 
 * @params_body
*/
export async function updateCategory(id : string, body: { name: string }) : Promise <any> {
    [rows] = await conn.promise().execute<RowDataPacket[]>(
        `UPDATE Categories \
        SET name = ? \
        WHERE id_category = ?`, [body.name, id]
    );
    return rows;
};

/** 
 * This function remove one category on database
 * @params_id 
*/
export async function deleteCategory(id : string) : Promise <any> {
    [rows] = await conn.promise().execute<RowDataPacket[]>("DELETE FROM Categories WHERE id_category = ?", [id]);
    return rows;
}

/** 
 * This function create a new category on database
 * @params_body
*/
export async function createCategory(body: ICategory) : Promise <any> {
    const {name} = body ;// exemple 
    [rows] = await conn.promise().execute<RowDataPacket[]>(
        `INSERT INTO Categories (name) VALUE (?)` , [name] // result of exemple , other way body.name
    );
    return rows;
}


conn.connect(function(err: any) { err ? err : console.log("Connecté à la base de données Mysql!"); });
