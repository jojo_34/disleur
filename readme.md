# **Project : Dis-leur !**

![Logo](ressources/images/Icon@3x.png)

### API Model : MVC

### Steps : 
* Clone Project : git clone https://gitlab.com/jojo_34/disleur.git
* Enter into project folder
```bash
cd dis_leur_repo/app
```
Install project dependecies
```bash
npm install
```
Docker compose deployment
```bash
docker-compose up -d
```
