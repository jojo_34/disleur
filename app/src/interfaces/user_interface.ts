export interface IUser {
    lastname: string;
    firstname: string;
    phone: number;
    mail: string;
    password: string;
    url_avatar: string;
}