import { conn } from "../config/config";

/** 
 * 
This function returns all comments found on the database posted by a user
 * @params_id 
*/
export async function getAllComments(id: string) : Promise <any> {

      const [rows] = await conn.promise().execute(
            `SELECT Comments.id_comment, Comments.text, Articles.title, Articles.id_article \
             FROM Comments \
             JOIN Articles \
             ON Comments.id_article = Articles.id_article \
             JOIN Users \
             ON Comments.id_user = Users.id_user WHERE Users.id_user = ?`, 
             [id] );
      return rows;
}

/** 
 * 
This function returns one comment found on the database posted by a user
 * @params_id 
*/
export async function getOneCommentById(id: string) : Promise <any> {

      const [rows] = await conn.promise().execute(
            `SELECT Comments.text, Articles.title, Articles.id_article \
             FROM Comments \
             JOIN Articles \
             ON Comments.id_article = Articles.id_article \
             JOIN Users \
             ON Comments.id_user = Users.id_user \
             WHERE id_comment = ?`, 
             [id]);
      return rows;
}


/** 
 * 
 *This function returns all comments on dataBase from one article 
 * @params_id 
*/
export async function getAllCommentByArticle(id: string) : Promise <any> {

      const [rows] = await conn.promise().execute(
            `SELECT Comments.id_comment, Comments.text, Articles.title, Articles.id_article \
             FROM Comments \
             JOIN Articles \
             ON Comments.id_article = Articles.id_article \
             JOIN Users \
             ON Comments.id_user = Users.id_user WHERE Articles.id_article = ? `, 
             [id] );
      return rows;
}

/** 
 * 
 *This function returns five last comments on dataBase from one article 
 * @params_id 
*/
export async function getLastCommentsByDate(id: string) : Promise <any> {

      const [rows] = await conn.promise().execute(
            `SELECT Comments.id_comment, Comments.text, Comments.date, Articles.title, Articles.id_article \
            FROM Comments JOIN Articles \
            ON Comments.id_article = Articles.id_article \
            JOIN Users ON Comments.id_user = Users.id_user \
            WHERE Articles.id_article = ? \
            ORDER BY Comments.date \
            DESC LIMIT 5;`, 
           [id] );
      return rows;
}

/** 
 * This function Update the text of one comment on database
 * @params_id 
 * @params_body
*/
export async function updateComment(id : string, body: { text: string }) : Promise <any> {

      const [rows] = await conn.promise().execute(
          `UPDATE Comments \
           SET text = ? \
           WHERE id_comment = ?`, 
           [body.text, id]
      );
      return rows;
}

/** 
 * This function Delete one comment on database
 * @params_id 
*/
export async function deleteComment(id : string) : Promise <any> {

      const [rows] = await conn.promise().execute(
            `DELETE FROM Comments WHERE id_comment = ?`, 
             [id]);
      return rows;
}

/** 
 * This function create a new comment on database
 * @params_body
*/
export async function createComment(body: any) : Promise <any> {
      
      const {text, url_site, date, id_article, id_user} = body;

      const [rows] = await conn.promise().execute(
            `INSERT INTO Comments (text, url_site, date, id_article, id_user) \
             VALUES (?, ?, ?, ?, ?)`, 
             [text, url_site, date, id_article, id_user]
      );
      console.log(rows);
      
      return rows;
}
