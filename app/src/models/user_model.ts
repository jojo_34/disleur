import { conn } from "../config/config"
// const bcrypt = require("bcrypt");

/**
 * This function return the users list found on database
 */
export async function getAllUsers() : Promise <any> {
    const [rows] = await conn.promise().execute(`SELECT * FROM Users`);
    return rows;
};

/**
 * This function return one user found on database
 */
export async function getOneUserById(id_user: string) : Promise <any> {
    const [rows] = await conn.promise().execute(
        `SELECT * FROM Users WHERE id_user = ?`,
         [id_user]
    );
    return [rows, id_user];
};

/**
 * This function Update the name of one user on database
 * @param id_user 
 * @param body 
 * @returns 
 */
export async function updateUser(id_user : string, body: {lastname : string, firstname : string, phone : number, mail : string, password : string, url_avatar : string}): Promise <any> {
    const [rows] = await conn.promise().execute(
        `UPDATE Users \
         SET lastname = ?, firstname = ?, phone = ?, mail = ?, password = ?, url_avatar = ?\
         WHERE id_user = ?`, [body.lastname, body.firstname, body.phone, body.mail, body.password, body.url_avatar, id_user]
    );
    return rows;
};

/**
 * This function remove one user on database
 * @param id_user 
 * @returns 
 */
export async function deleteUser(id_user : string) : Promise <any> {
    const [rows] = await conn.promise().execute(
        `DELETE FROM Users WHERE id_user = ?`,
        [id_user]
    );
    return rows;
};

/**
 * This function create a new user on database
 * @param body 
 * @returns 
 */
// export async function createUser(name : string, first_name : string, phone : number, mail : string, 
// password : string, url_avatar : string): Promise <any> {
export async function createUser(body : any): Promise <any> {
    const {lastname, firstname, phone, mail, password, url_avatar} = body;
    const [rows] = await conn.promise().execute(
        `INSERT INTO Users (lastname, firstname, phone, mail, password, url_avatar) VALUES (?, ?, ?, ?, ?, ?)`,
        [lastname, firstname, phone, mail, password, url_avatar]
    );
    return rows;
};

conn.connect(function(err: any) { err ? err : console.log("Connecté à la base de données Mysql!"); });