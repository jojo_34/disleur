-- MySQL dump 10.13  Distrib 8.2.0, for Linux (x86_64)
--
-- Host: localhost    Database: disleur_db
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `disleur_db`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `disleur_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `disleur_db`;

--
-- Table structure for table `Articles`
--

DROP TABLE IF EXISTS `Articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Articles` (
  `id_article` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `text` text NOT NULL,
  `date` date NOT NULL,
  `like_qty` int unsigned NOT NULL,
  `share_qty` int unsigned NOT NULL,
  `state_article` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_article`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Articles`
--

LOCK TABLES `Articles` WRITE;
/*!40000 ALTER TABLE `Articles` DISABLE KEYS */;
INSERT INTO `Articles` VALUES (1,'En attendant la loi grand âge\"... : En Occitanie, 141 000 senior','La quasi-totalité des seniors (96 %) vivent ainsi à leur domicile, soit 1,7 million de personnes. Et un quart des seniors vivant à domicile font néanmoins face à au moins une limitation fonctionnelle en Occitanie où il y a moins de places en Ehpad que dans d autres régions. Mais où une offre de soins infirmiers est très développée.','2024-01-05',1,1,1),(2,'France 2030 : Même la licorne des droïdes de livraison a besoin ','Basée à Cahors, Twinswheel est l une des lauréates de ce plan de réindustrialisation dont Emmanuel Macron a fait un premier bilan à Toulouse, ce lundi. Avoir une très bonne idée et des compétences ne suffisent pas toujours... Macron, lui, a fait du Macron. Soulignant qu il faut créer les champions de demain.','2024-01-05',2,2,2),(3,'titre Bidon','text X Bidon','2024-01-10',0,0,1);
/*!40000 ALTER TABLE `Articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Articles_categories`
--

DROP TABLE IF EXISTS `Articles_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Articles_categories` (
  `id_articles_categories` int unsigned NOT NULL AUTO_INCREMENT,
  `id_article` int unsigned NOT NULL,
  `id_category` int unsigned NOT NULL,
  PRIMARY KEY (`id_articles_categories`),
  KEY `id_article` (`id_article`),
  KEY `id_categories` (`id_category`),
  CONSTRAINT `Articles_categories_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `Articles` (`id_article`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Articles_categories_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `Categories` (`id_category`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Articles_categories`
--

LOCK TABLES `Articles_categories` WRITE;
/*!40000 ALTER TABLE `Articles_categories` DISABLE KEYS */;
INSERT INTO `Articles_categories` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `Articles_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Articles_users`
--

DROP TABLE IF EXISTS `Articles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Articles_users` (
  `id_article_user` int unsigned NOT NULL AUTO_INCREMENT,
  `id_article` int unsigned NOT NULL,
  `id_user` int unsigned NOT NULL,
  PRIMARY KEY (`id_article_user`),
  KEY `id_article` (`id_article`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `Articles_users_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `Articles` (`id_article`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Articles_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Articles_users`
--

LOCK TABLES `Articles_users` WRITE;
/*!40000 ALTER TABLE `Articles_users` DISABLE KEYS */;
INSERT INTO `Articles_users` VALUES (1,2,2),(2,3,1);
/*!40000 ALTER TABLE `Articles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Categories`
--

DROP TABLE IF EXISTS `Categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Categories` (
  `id_category` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categories`
--

LOCK TABLES `Categories` WRITE;
/*!40000 ALTER TABLE `Categories` DISABLE KEYS */;
INSERT INTO `Categories` VALUES (1,'Entretien'),(2,'Société');
/*!40000 ALTER TABLE `Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Comments`
--

DROP TABLE IF EXISTS `Comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Comments` (
  `id_comment` int unsigned NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `url_site` varchar(512) NOT NULL,
  `date` date NOT NULL,
  `id_article` int unsigned NOT NULL,
  `id_user` int unsigned NOT NULL,
  PRIMARY KEY (`id_comment`),
  KEY `id_article` (`id_article`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `Comments_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `Articles` (`id_article`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Comments_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comments`
--

LOCK TABLES `Comments` WRITE;
/*!40000 ALTER TABLE `Comments` DISABLE KEYS */;
INSERT INTO `Comments` VALUES (1,' MODIF : onsectetur bal adipiscing elit. Morbi volutpat velit sed sem tempor porttitor mattis, neque velit ullamcorper justo, quis fermentum risus dolor in elit.','none','2024-01-05',2,1),(2,'commentaire 2','https://dis-leur.fr','2024-01-02',3,1),(3,'ut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut','aucun','2024-01-14',2,2),(5,'ut odit aut ','aucun','2024-01-16',1,1),(6,'because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely pain, sed quia consequuntur magni dolores eos qui ratione voluptatem seq','aucun','2024-01-17',1,2),(7,'because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely pain, sed quia consequuntur magni dolores eos qui ratione voluptatem seq','aucun','2024-01-18',1,2),(8,'because it is pleasure, but because those who do not know how to pursue  rationally encounter consequences that are extremely pain, sed quia  magni dolores eos qui ratione voluptatem seq','aucun','2024-01-18',1,2),(9,'because it is pleasure, but ces that are extremely pain, sed quia  magni dolores eos qui ratione voluptatem seq','aucun','2024-01-19',1,2),(10,'because it is pleasure, but ces  seq','aucun','2024-01-19',1,1);
/*!40000 ALTER TABLE `Comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Favorites`
--

DROP TABLE IF EXISTS `Favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Favorites` (
  `id_favorite` int unsigned NOT NULL AUTO_INCREMENT,
  `id_article` int unsigned NOT NULL,
  `id_user` int unsigned NOT NULL,
  PRIMARY KEY (`id_favorite`),
  KEY `id_article` (`id_article`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `Favorites_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `Articles` (`id_article`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Favorites_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Favorites`
--

LOCK TABLES `Favorites` WRITE;
/*!40000 ALTER TABLE `Favorites` DISABLE KEYS */;
INSERT INTO `Favorites` VALUES (1,2,2),(2,3,2);
/*!40000 ALTER TABLE `Favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Pictures`
--

DROP TABLE IF EXISTS `Pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Pictures` (
  `id_picture` int unsigned NOT NULL AUTO_INCREMENT,
  `picture_title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `legend` varchar(255) NOT NULL,
  `meta_data` text NOT NULL,
  `url` varchar(512) NOT NULL,
  `id_article` int unsigned NOT NULL,
  PRIMARY KEY (`id_picture`),
  UNIQUE KEY `picture_title` (`picture_title`),
  KEY `id_article` (`id_article`),
  CONSTRAINT `Pictures_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `Articles` (`id_article`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pictures`
--

LOCK TABLES `Pictures` WRITE;
/*!40000 ALTER TABLE `Pictures` DISABLE KEYS */;
INSERT INTO `Pictures` VALUES (1,'titre article 1','legend article 1','blablabla','https://magazine.sportihome.com/wp-content/uploads/2021/02/montagne-ete-scaled.jpg',2),(2,'titre article 2','legend article 2','blobloblo','https://magazine.sportihome.com/wp-content/uploads/2021/02/vacances-montagnes-tignes.jpg',3);
/*!40000 ALTER TABLE `Pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subscriptions`
--

DROP TABLE IF EXISTS `Subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Subscriptions` (
  `id_subs` int unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `price` decimal(64,0) NOT NULL,
  PRIMARY KEY (`id_subs`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subscriptions`
--

LOCK TABLES `Subscriptions` WRITE;
/*!40000 ALTER TABLE `Subscriptions` DISABLE KEYS */;
INSERT INTO `Subscriptions` VALUES (1,'Mensuel',10),(2,'Annuel',100);
/*!40000 ALTER TABLE `Subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `id_user` int unsigned NOT NULL AUTO_INCREMENT,
  `lastname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `firstname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phone` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `mail` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `url_avatar` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Spengler','Egon','09-08-07-06-05','e.s@g.com','$2y$10$t3iwC8BPZ8wG3GyiV3NwL.wXRpRmPA0BtysaDRGkEZPn8CQjOW6rS','https://robohash.org/cupiditatequisapiente.png?size=300x300'),(2,'Melnitz','Janine','09-08-07-06-04','j.m@g.com','$2y$10$yEXkZrKyoE0Hd0htrzxqFOM4O4NijbebfXpHF6bMwzHoJTCpXjYL6','https://robohash.org/culpavitaequasi.png?size=300x300'),(3,'Venkman','Peter','09-08-07-16-14','p.v@g.com','$2y$10$BZNCN9NHCenEN9rV50jp.ODK17kq1OOiMCII1wWMmcVIPV9yc1kwW','https://upload.wikimedia.org/wikipedia/commons/7/72/Bill_Murray_Cannes_2019.jpg');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_subscriptions`
--

DROP TABLE IF EXISTS `Users_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users_subscriptions` (
  `id_user_subs` int unsigned NOT NULL AUTO_INCREMENT,
  `date_of_subs` date NOT NULL,
  `deadline` date NOT NULL,
  `state` tinyint(1) NOT NULL,
  `id_user` int unsigned NOT NULL,
  `id_subs` int unsigned NOT NULL,
  PRIMARY KEY (`id_user_subs`),
  KEY `id_user` (`id_user`),
  KEY `id_subs` (`id_subs`),
  CONSTRAINT `Users_subscriptions_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Users_subscriptions_ibfk_4` FOREIGN KEY (`id_subs`) REFERENCES `Subscriptions` (`id_subs`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_subscriptions`
--

LOCK TABLES `Users_subscriptions` WRITE;
/*!40000 ALTER TABLE `Users_subscriptions` DISABLE KEYS */;
INSERT INTO `Users_subscriptions` VALUES (1,'2024-01-12','2024-02-12',1,1,2),(2,'2024-01-12','2025-01-12',1,2,1);
/*!40000 ALTER TABLE `Users_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-04  8:03:03
