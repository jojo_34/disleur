import {conn} from "../config/config";
import { RowDataPacket } from 'mysql2';
import bcrypt from 'bcryptjs';
import jsonwebtoken from 'jsonwebtoken';
import { Request, Response, NextFunction} from "express";
// import { Reqpayload } from '../interfaces/req_payload_interface';

const { JWT_SECRET }: any = process.env;

/** 
 * 
This function returns id_user and password of Users and return token if auth is ok
 * @params_body
*/

// Asynchronous function to handle user authentication
export async function signIn(body: any): Promise<any> {
      // Extract 'mail' and 'password' fields from the request body
      const { mail, password } = body;

      // Declare a variable to store the results of the SQL query
      let rows: RowDataPacket[] = [];

      // Execute the SQL query to retrieve the user's ID and password based on the provided email
      [rows] = await conn.promise().execute<RowDataPacket[]>(
            `SELECT id_user, password \
             FROM Users \
             WHERE mail = ?`, [mail]);
     
      // Check if any results were returned by the query
      if (rows.length) {
            // Retrieve the user's ID and password from the query results
            let idUser = rows[0].id_user;
            let pwdBd = rows[0].password;
            
            // Compare the provided password with the one stored in the database using bcrypt
            if (await bcrypt.compare(password, pwdBd)) {
                  // If passwords match, generate a JWT token for the user
                  const token = jsonwebtoken.sign({ idUser }, JWT_SECRET, { expiresIn: "1h" })
                  // Return the JWT token in an object
                  return { token: token };                  
            } else {
                  // If passwords do not match, return an error message
                  return { "message": "Incorrect password" };             
            }
      } else {
            // If no user matching the email was found, return an error message
            return { "message": "Incorrect email" };
      }
}
/** 
 * This function verifies the authenticity of a token and returns either the decoded payload or false.
 */
export function verifyAccessToken(token: any) {
    
      try {
            // Verify the token using the JWT_SECRET
            const decoded = jsonwebtoken.verify(token, JWT_SECRET);
            // If verification is successful, return success and the decoded payload
            return { success: true, data: decoded };
      } catch (error) {
            // If verification fails, return failure
            return { success: false };
      }
};

/** 
 * This function checks if there is a token and verifies its authenticity.
 */
export interface Reqpayload extends Request {
      // Define the payload attribute to hold the decoded token payload
      payload?: string | jsonwebtoken.JwtPayload ;
}

// Middleware function to authenticate the token
export function authenticateToken(req: Reqpayload, res: Response, next: NextFunction) {

      // Extract the token from the request headers
      const authHeader = req.headers.authorization; 
      const token = authHeader && authHeader.split(' ')[1];

      // Check if a token is present
      if (!token) {
            // If no token is found, return a 400 error
            return res.status(400).json({ message: 'auth-token missing' })      
      }
    
      // Verify the authenticity of the token
      const result = verifyAccessToken(token);
      
      // If token verification fails, return a 401 error
      if (!result.success) {
            return res.status(401).json({ message: 'Invalid token' })      
      }
      
      // Attach the decoded token payload to the request object
      (req as Reqpayload).payload = result.data;
      
      // Call the next middleware function
      next();
}
