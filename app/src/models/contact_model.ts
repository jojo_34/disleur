import { conn } from "../config/config";

/** 
 * This function return all from one user
 * @param id 
*/
export async function getUserById(id_user : string ): Promise <any> {
    const [rows] = await conn.promise().execute(
        `SELECT * FROM Users WHERE id_user = ?`,
         [id_user]
    );
    return rows;
};

/** 
 * This function return user's name
 * @param id 
*/
export async function getUserByName(id_user : string ): Promise <any> {
    const [rows] = await conn.promise().execute(
        `SELECT lastname FROM Users WHERE id_user = ?`,
         [id_user]
    );
    return rows;
};

/** 
 * This function return user's first name
 * @param id 
*/
export async function getUserByFirstName(id_user : string ): Promise <any> {
    const [rows] = await conn.promise().execute(
        `SELECT firstname FROM Users WHERE id_user = ?`,
         [id_user]
    );
    return rows;
};

/** 
 * This function return user's phone number
 * @param id 
*/
export async function getUserByPhone(id_user : string ): Promise <any> {
    const [rows] = await conn.promise().execute(
        `SELECT phone FROM Users WHERE id_user = ?`,
         [id_user]
    );
    return rows;
};

/** 
 * This function return user's mail
 * @param id 
*/
export async function getUserByMail(id_user : string ): Promise <any> {
    const [rows] = await conn.promise().execute(
        `SELECT mail FROM Users WHERE id_user = ?`,
         [id_user]
    );
    return rows;
};

export async function updateNameFromUser(id_user : string , body : {lastname : string}) : Promise <any> {
    const [rows] = await conn.promise().execute(
        `UPDATE Users SET lastname = ? WHERE id_user = ?`,
        [body.lastname, id_user]
    );
    return rows;
};

export async function updateFirstNameFromUser(id_user : string, body : {firstname : string}) : Promise <any> {
    const [rows] = await conn.promise().execute(
        `UPDATE Users SET firstname = ? WHERE id_user = ?`,
        [body.firstname, id_user]
    );
    return rows;  
};

export async function updatePhoneFromUser(id_user : string, body : {phone : string}) : Promise <any> {
    const [rows] = await conn.promise().execute(
        `UPDATE Users SET phone = ? WHERE id_user = ?`,
        [body.phone, id_user]
    );
    return rows;
};

export async function updateMailFromUser(id_user : string, body : {mail : string}) : Promise <any> {
    const [rows] = await conn.promise().execute(
        `UPDATE Users SET mail = ? WHERE id_user = ?`,
        [body.mail, id_user]
    );
    return rows;
};