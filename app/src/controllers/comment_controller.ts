import { Request, Response } from "express";
import * as Comments from "../models/comment_model";
import { Reqpayload } from '../models/auth_model';

/** 
This function returns all comments posted by a user
 * Used into the profil user
 * @params_id 
*/
export function getAllComments(req : Request, res : Response){

      // console.log('Controller : ', (req as Reqpayload).payload); // For test , will be removed later.
      Comments.getAllComments(req.params.id)
      .then((rows : any)=> {

            if (!rows[0]){

                  res.status(207).json({ "message" : "Aucun utilisateur" });
            } else {

                  res.json(rows);
            }
     })     
     .catch((err : any)=>res.status(500).send(err));
}

/** 
This function returns one comments posted by a user
 * Used into the profil user
 * @params_id 
*/
export function getOneCommentById(req : Request, res : Response){

      Comments.getOneCommentById(req.params.id)
      .then((rows : any)=> {

            if (!rows[0]){

                  res.status(207).json({ "message" : "Aucun commentaire" });
            } else {

                  res.json(rows);
            }
     })
     .catch((err : any)=>res.status(500).send(err));
}

/** 
This function returns all comments related to an article
 * @params_id 
*/
export function getAllCommentByArticle(req : Request, res : Response){

      Comments.getAllCommentByArticle(req.params.id)
      .then((rows : any)=> {

            if (!rows[0]){

                  res.status(207).json({ "message" : "Aucun article" });
            } else {

                  res.json(rows);
            }
     })     
     .catch((err : any)=>res.status(500).send(err));
}

/** 
This function returns last five comments related to an article
 * @params_id 
*/
export function getLastCommentsByDate(req : Request, res : Response){

      Comments.getLastCommentsByDate(req.params.id)
      .then((rows : any)=> {

            if (!rows[0]){

                  res.status(207).json({ "message" : "Aucun article" });
            } else {

                  res.json(rows);
            }
     })     
     .catch((err : any)=>res.status(500).send(err));
}

/** 
 * This function update the text of one comment
 * Used into the profil user
 * @params_id 
 * @params_body
*/
export function updateComment(req : Request, res : Response){
      const updateCommentContent = req.body;
      Comments.updateComment(req.params.id, req.body)    
      .then((rows : any)=>{

            if (rows.affectedRows === 0){

                  res.status(404).json({ "message" : 'Aucun commmentaire disponible !' });
            } else {

                  res.json({ updateCommentContent, "message" : 'Commentaire modifié !' });
            }
      })
      .catch((err : any)=>res.status(500).send(err));
}

/** 
 * This function remove one comment
 * Used into the profil user
 * @params_id 
*/
export function deleteComment(req : Request, res : Response){      
      Comments.deleteComment(req.params.id)
      .then((rows : any)=>{

            console.log('rows.affectedRows :', rows.affectedRows);

            if (rows.affectedRows === 0){

                  res.status(404).json({ "message" : 'Aucun commmentaire à supprimer !' });
            } else {

                  res.status(207).json({ "message" : "Commentaire supprimé !" })
            }
      })
      .catch((err : any)=>res.status(500).send(err));
}

/** 
 * This function create a new comment
 * Used into the profil user
 * @params_body
*/
export function createComment(req : Request, res : Response){
      const commentContent = req.body;
      Comments.createComment(req.body)
      .then((rows : any)=>{

            if (rows.affectedRows === 0){

                  res.status(404).json({ "message" : 'Le commentaire n\'a pas pu être ajouté !' });
            } else { 

                  res.json({ commentContent, "message" : 'Commentaire ajouté !' });
            }
      })
      .catch((err : any)=>res.status(500).send(err));
}
