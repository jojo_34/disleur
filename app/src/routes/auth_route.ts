import { Router } from 'express';
const authRouter = Router();
import * as controller from '../controllers/auth_controller';

authRouter.post("/signin", controller.signIn);

export default authRouter;
